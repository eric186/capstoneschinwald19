package main;

import eps.NewEpisodes;
import gui.Gui;
import importer.WatchImporter;

public class Main {

	private static WatchImporter importer;
	private static NewEpisodes newEps;
	private static Thread newEpisodesThread;
	private static Thread importerThread;
	private static final Gui gui = new Gui();
	
	public static void main(String[] args) {
		Main m = new Main();
		m.start();
		
	}
	
	private void start() {
		Thread guiThread = new Thread(gui);
		guiThread.start();
	}
	
	public static void startNewEpisodes() {
		newEps = new NewEpisodes();
		newEpisodesThread = new Thread(newEps);
		newEpisodesThread.start();
	}
	
	//Let's the gui start the import after the credentials have been entered
	public static void startImport(String username, String password, String profile) {
		importer = new WatchImporter(username, password, profile);
		importerThread = new Thread(importer);
		importerThread.start();
		
	}
	
	public static boolean isImporterEpisodesAlive() {
		if(newEpisodesThread != null && newEpisodesThread.isAlive()) {
			return true;
		}else if(importerThread != null && importerThread.isAlive()) {
			return true;
		}
		return false;
	}
	
	public static void retryLogIn() {
		gui.retryLogIn();	
	}
	
}
