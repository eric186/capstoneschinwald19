package eps;

public class Episode implements Comparable<Episode>{
	private String show;
	private int season;
	private int epNr;
	private String name;
	
	public Episode(String show, int season, int epNr, String name) {
		this.show = show;
		this.season = season;
		this.epNr = epNr;
		this.name = name;
	}
	@Override
	public String toString() {
		return "Show: " + show + ", Season: " + season + ", EpisodeNr: " + epNr + ", Name: " + name + "\n";
	}
	
	public int compareTo(Episode e) {
		if(this.show.compareToIgnoreCase(e.show)<0) {
			return -1;
		}else if(this.show.compareToIgnoreCase(e.show)>0) {
			return 1;
		}else {
			if(this.season<e.season) {
				return -1;
			}else if(this.season>e.season) {
				return 1;
			}else {
				if(this.epNr<e.epNr) {
					if(this.name.equals(e.name)) {
						return 0;
					}else {
						return -1;
					}
				}else if(this.epNr>e.epNr) {
					if(this.name.equals(e.name)) {
						return 0;
					}else {
						return 1;
					}
				}else {
					return 0;
				}
			}
		}
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof Episode)) {
			return false;
		}
		Episode e = (Episode) o;
		if(this.show==e.show&&this.season==e.season) {
			if(this.epNr!=e.epNr) {
				if(this.name==e.name) {
					return true;
				}else {
					return false;
				}
			}else {
				return true;
			}
		}
		return false;
	}
	
	public String getShow() {
		return show;
	}
	
	public int getSeason() {
		return season;
	}
	
	public int getEpNr() {
		return epNr;
	}
	
	public String getName() {
		return name;
	}

}
