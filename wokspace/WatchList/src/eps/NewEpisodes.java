package eps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import db.DBInterface;
import gui.Gui;

public class NewEpisodes implements Runnable{
	
	DBInterface db = DBInterface.getInstance();
	
	
	private String getRequest(URL urlForGetRequest) {
		String s = null;
		try {
		    String readLine = null;
		    HttpURLConnection conection;
			conection = (HttpURLConnection) urlForGetRequest.openConnection();
			
		    conection.setRequestMethod("GET");
		    int responseCode = conection.getResponseCode();
		    if (responseCode == HttpURLConnection.HTTP_OK) {
		        BufferedReader in = new BufferedReader(
		            new InputStreamReader(conection.getInputStream()));
		        StringBuffer response = new StringBuffer();
		        while ((readLine = in .readLine()) != null) {
		            response.append(readLine);
		        } in .close();
		        s = response.toString();
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}
	
	
	@Override
	public void run() {
		List<String> watchedStrList = db.getWatched(false);
		List<Episode> watchedList = translateWatchList(watchedStrList);
		Map<Episode, Boolean> episodeMap = new TreeMap<Episode, Boolean>();
		System.out.println(watchedList);
		Set<String> tvShows = new HashSet<String>();
		for(String element: watchedStrList) {
			String tvShow = element.substring(0, element.indexOf(":"));
			tvShow = tvShow.replace(" ", "+");
			tvShows.add(tvShow);
		}
		for(String tvShow: tvShows) {
			try {
				String response = getRequest(new URL("http://api.tvmaze.com/search/shows?q=" + tvShow));
				if(response != null) {
					try {
				        JSONArray jsonArray = new JSONArray(response.toString());
				        JSONObject jsonObj = jsonArray.getJSONObject(0);
				        JSONObject jsonShow = jsonObj.getJSONObject("show");
				        int id = jsonShow.getInt("id");
				        System.out.println("ID:" + id);
				        String episodeList = getRequest(new URL("http://api.tvmaze.com/shows/" + id + "/episodes?specials=0"));
				        if(episodeList == null) {
				        	continue;
				        }
				        jsonArray = new JSONArray(episodeList.toString());
				        for(int i = 0; i < jsonArray.length(); i++) {
				        	jsonObj = jsonArray.getJSONObject(i);
				        	//return tvshow to old representation
				        	tvShow = tvShow.replace("+", " ");
				        	Episode ep = new Episode(tvShow, jsonObj.getInt("season"),jsonObj.getInt("number"),jsonObj.getString("name"));
				        	episodeMap.put(ep, false);
				        }
			        }catch(JSONException e) {
			        	System.err.println(tvShow + " not found on TV Maze");
			        }
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		Map<Episode, Boolean> markedMap = compare(episodeMap, watchedList);
		save(markedMap);
		Gui.fireWatchedChanged();
	}
	
	private void save(Map<Episode, Boolean> markedMap) {
		for (Map.Entry<Episode, Boolean> entry : markedMap.entrySet()) {
			if(!entry.getValue()) {
				Episode key = entry.getKey();
				db.addUnwatched(key.getShow(), key.getSeason(), key.getEpNr(), key.getName());
			}
		}
	}
	
	private Map<Episode, Boolean> compare(Map<Episode, Boolean> map, List<Episode> watchedList){
		for(Episode watched: watchedList) {
			if(map.containsKey(watched)) {
				map.put(watched, true);
			}
		}
		return map;
	}
	
	private List<Episode> translateWatchList(List<String> watchedStrList){
		List<Episode> episodes = new ArrayList<Episode>();
		for(String element:watchedStrList) {
			String[] sArray = element.split(":");
			int seasonNr = -1;
			int episodeNr = -1;
			String epName = "";
			if(sArray[1].startsWith("Season")||sArray[1].startsWith("Part")||sArray[1].startsWith("Series")||sArray[1].startsWith("Volume")) {
				seasonNr = Integer.parseInt(sArray[1].substring(sArray[1].indexOf(' ')+1));
			}
			if(sArray[2].startsWith("Episode")) {
				episodeNr = Integer.parseInt(sArray[2].substring(sArray[2].indexOf(' ')+1));
			}else {
				epName = sArray[2];
			}
			episodes.add(new Episode(sArray[0], seasonNr, episodeNr, epName));
		}
		return episodes;
	}

}
