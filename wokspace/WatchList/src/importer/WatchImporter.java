package importer;

import java.util.List;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.machinepublishers.jbrowserdriver.JBrowserDriver;

import db.DBInterface;
import gui.Gui;
import main.Main;

public class WatchImporter implements Runnable{
	private static JBrowserDriver driver;
	private DBInterface db = DBInterface.getInstance();
	private final String usernameStr;
	private final String passwordStr;
	private final String profileStr;
	public static int codeNr = -1;
	
	public WatchImporter(String username, String password, String profile) {
		this.usernameStr = username;
		this.passwordStr = password;
		this.profileStr = profile;
	}
	
	@Override
	public void run() {
		if(runNetflix()) {
			Main.retryLogIn();
		}
	}
	
	
	//return means that a retry should be attempted
	private boolean runNetflix() {
		driver = new JBrowserDriver();
		driver.get("http://netflix.com/login");
		WebElement username = driver.findElementById("id_userLoginId");
		username.sendKeys(usernameStr);
		WebElement password = driver.findElementById("id_password");
		password.click();
		password.sendKeys(passwordStr);
		WebElement button = driver.findElementByXPath("//button[@type=\"submit\"]");
		button.click();
		try {
			WebElement profileAnchor = driver.findElementByXPath("//a[span=\""+ profileStr +"\"]");
			profileAnchor.click();
		}catch (NoSuchElementException | ElementNotVisibleException e) {
			return true;
		}
		
		driver.get("https://www.netflix.com/viewingactivity");
		while(true){
			try {
			WebElement showButton = driver.findElementByXPath("//button[@class=\"btn btn-blue btn-small\"]");
			showButton.click();
			}catch(NoSuchElementException e) {
				break;
			}
		}
		List<WebElement> episodes = driver.findElementsByXPath("//li/div[@class=\"col title\"]/a");
		for(WebElement episode: episodes) {
			String[] s= episode.getText().split(":");
			if(s.length == 3) {
				s[2] = s[2].substring(2, s[2].length()-1);
				s[1] = s[1].substring(1);
				db.addEpisode(s[0], s[1], s[2]);
			}
		}
		
		driver.quit();
		Gui.fireWatchedChanged();
		Main.startNewEpisodes();
		return false;
	}

}
