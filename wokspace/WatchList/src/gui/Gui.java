package gui;

import java.util.List;
import java.util.Optional;

import db.DBInterface;
import main.Main;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class Gui extends Application implements Runnable{

	private static DBInterface db = DBInterface.getInstance();
	private static ListView<String> watchedList;
	private static ListView<String> unwatchedList;
	private static volatile Stage stage;
	private static volatile boolean watchedDescending = false;
	private static volatile boolean unwatchedDescending = false;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		    @Override
		    public void handle(WindowEvent e) {
		     Platform.exit();
		     System.exit(0);
		    }
		  });
		createNetflixLogin(primaryStage);
	}
	/*private void serviceSelection(Stage primaryStage) {
		VBox services = new VBox();
		Button netflixBtn = new Button("Netflix");
		
		netflixBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
			createNetflixLogin(primaryStage);
		});
		
		netflixBtn.setPrefWidth(200);
		services.getChildren().add(netflixBtn);
		GridPane pane = new GridPane();
		pane.setHgap(10);
	    pane.setVgap(5);
	    pane.addColumn(1, services);
		Scene scene = new Scene(pane);
		 
		primaryStage.setTitle("Service Selection");
		primaryStage.setScene(scene);
		primaryStage.show();
	}*/
	
	public void retryLogIn() {
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Invalid Login Credentials");
				alert.setHeaderText(null);
				alert.setContentText("There seems to be a problem with either your username, password or profile name. Do you want to retry?");
	
				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK){
				    createNetflixLogin(stage);
				}
			}
		});
		
	}
	private void createNetflixLogin(Stage primaryStage) {
		
		TextField username = new TextField();
		PasswordField password = new PasswordField();
		TextField profile = new TextField();
		Button submitButton = new Button("Submit");
		
		Label usernameLbl = new Label("Username: ");
		Label passwordLbl = new Label("Password: ");
		Label profileLbl = new Label("Profile: ");
		
		submitButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
			Main.startImport(username.getText(), password.getText(), profile.getText());
			createMainView(primaryStage);
		});
		
		GridPane pane = new GridPane();
		pane.setPadding(new Insets(10, 10, 10, 10));
	    pane.setHgap(10);
	    pane.setVgap(5);
	    pane.add(usernameLbl, 0, 0);
	    pane.add(passwordLbl, 0, 1);
	    pane.add(profileLbl, 0, 2);
	    pane.add(username, 1, 0);
	    pane.add(password, 1, 1);
	    pane.add(profile, 1, 2);
	    pane.add(submitButton, 0, 3, 2, 1);
		//pane.addColumn(1, form);
	    
		Scene scene = new Scene(pane);
		
		primaryStage.setTitle("Netflix login");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private void createMainView(Stage primaryStage) {
		Label watchedLbl = new Label("List of watched episodes");
		
		Label unwatchedLbl = new Label("List of unwatched epsodes");
		
		watchedLbl.setStyle("-fx-font-weight: bold");
		unwatchedLbl.setStyle("-fx-font-weight: bold");
		watchedList = getWatched();
		unwatchedList = getUnwatched();
		
		Button watchedOrderBtn = new Button("A-Z");
		Button unwatchedOrderBtn = new Button("A-Z");
		
		//events
		watchedOrderBtn.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
			if(watchedOrderBtn.getText().equals("A-Z")) {
				watchedOrderBtn.setText("Z-A");
				watchedDescending = true;
			}else {
				watchedOrderBtn.setText("A-Z");
				watchedDescending = false;
			}
			fireWatchedChanged();
		});
		unwatchedOrderBtn.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
			if(unwatchedOrderBtn.getText().equals("A-Z")) {
				unwatchedOrderBtn.setText("Z-A");
				unwatchedDescending = true;
			}else {
				unwatchedOrderBtn.setText("A-Z");
				unwatchedDescending = false;
			}
			fireWatchedChanged();
		});
		Button netflixImport = new Button("Retry import of Episodes");
		netflixImport.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
			if(!Main.isImporterEpisodesAlive()) {
				createNetflixLogin(primaryStage);
			}else {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Import of Netflix is currently running");
				alert.setHeaderText(null);
				alert.setContentText("The import of the Netflix viewing history is currently running. Please wait for it to finish to be able to retry.");
	
				alert.showAndWait();
			}
			
		});
		
		  // Create the GridPane
        GridPane pane = new GridPane();
        
        GridPane.setHalignment(netflixImport, HPos.CENTER);
        GridPane.setHalignment(watchedOrderBtn, HPos.CENTER);
        GridPane.setHalignment(unwatchedOrderBtn, HPos.CENTER);
        
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.setHgap(10);
        pane.setVgap(5);
        pane.add(watchedLbl, 0, 0);
        pane.add(watchedList, 0, 1);
        pane.add(watchedOrderBtn, 0, 2);
        pane.add(unwatchedLbl, 1, 0);
        pane.add(unwatchedList, 1, 1);
        pane.add(unwatchedOrderBtn, 1, 2);
		pane.add(netflixImport, 0, 3, 2, 1);
		
		Scene scene = new Scene(pane);
		
		primaryStage.setTitle("WatchList"); 
		primaryStage.setScene(scene);
		primaryStage.show();
		Gui.stage = primaryStage;
	}

	private static ListView<String> getWatched() {
		List<String> list = db.getWatched(watchedDescending);
		ObservableList<String> obsList = FXCollections.observableArrayList(list);
		ListView<String> watched = new ListView<String>(obsList);
		return watched;
	}
	
	private static ListView<String> getUnwatched() {
		List<String> list = db.getUnwatched(unwatchedDescending);
		ObservableList<String> obsList = FXCollections.observableArrayList(list);
		ListView<String> unWatched = new ListView<String>(obsList);
		return unWatched;
	}
	
	public synchronized static void fireWatchedChanged() {
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				ObservableList<String> obsList = watchedList.getItems();
				obsList.clear();
				List<String> list = db.getWatched(watchedDescending);
				obsList.addAll(list);
				obsList = unwatchedList.getItems();
				obsList.clear();
				list = db.getUnwatched(unwatchedDescending);
				obsList.addAll(list);
			}
		});
		
	}
	
	@Override
	public void run() {
		launch();
	}
}
