package db;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DBInterface {
	
	private static DBInterface instance = null; 
	public static String path = "db/data.db";
	public static String folder = "db";
	public static String url = "jdbc:sqlite:" + path; 
	
	public DBInterface() {
		File folderFile = new File(folder);
		if(!folderFile.exists()) {
				folderFile.mkdir();
		}
		File dbFile = new File(path);
		if(!dbFile.exists()) {
			try {  
	            Connection conn = DriverManager.getConnection(url);  
	            if (conn != null) {  
	                DatabaseMetaData meta = conn.getMetaData();  
	                System.out.println("The driver name is " + meta.getDriverName());  
	                System.out.println("A new database has been created.");  
	            }  
	            // SQL statement for creating a new table  
	            String watchListSql = "CREATE TABLE IF NOT EXISTS WATCHLIST (\n"    
	                    + " SHOW TEXT,\n" 
	                    + " SEASON TEXT,\n"
	                    + " EPISODE TEXT,\n"
	                    + " primary key (SHOW, SEASON, Episode)\n"
	                    + ");";  
	               
                Statement stmt = conn.createStatement();  
                stmt.execute(watchListSql); 
                
                String unwatchedSql = "CREATE TABLE IF NOT EXISTS UNWATCHED (\n"    
	                    + " SHOW TEXT,\n" 
	                    + " SEASON INTEGER,\n"
	                    + " EPISODE INTEGER,\n"
	                    + " NAME TEXT,\n"
	                    + " primary key (SHOW, SEASON, Episode)\n"
	                    + ");";
                stmt = conn.createStatement();
                stmt.execute(unwatchedSql);
    	        conn.close();
	        } catch (SQLException e) {  
	           e.printStackTrace();
	        }
			
			
		}
	}
	
	public static void resetInstance() {
		instance = new DBInterface();
	}
	
	  // static method to create instance of DBInterface
    public synchronized static DBInterface getInstance() 
    { 
        if (instance == null) {
            instance = new DBInterface();
        }
        return instance; 
    } 
	
	public synchronized void addEpisode(String show, String season, String episode) {
		try {
			Connection conn = DriverManager.getConnection(url); 	
	        
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO WATCHLIST (SHOW, SEASON, EPISODE) values (?, ?, ?);");
	        stmt.setString(1, show);
	        stmt.setString(2, season);
	        stmt.setString(3, episode);
	        stmt.executeUpdate();
	        conn.close();
		}catch(SQLException e) {
			if(!String.valueOf(e.getErrorCode()).startsWith("19")) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void addUnwatched(String show, int season, int episode, String name) {
		try {
			Connection conn = DriverManager.getConnection(url); 	
	        
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO UNWATCHED (SHOW, SEASON, EPISODE, NAME) values (?, ?, ?, ?);");
	        stmt.setString(1, show);
	        stmt.setInt(2, season);
	        stmt.setInt(3, episode);
	        stmt.setString(4, name);
	        stmt.executeUpdate();
	        conn.close();
		}catch(SQLException e) {
			if(!String.valueOf(e.getErrorCode()).startsWith("19")) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized List<String> getWatched(boolean descending){
		List<String> list = new ArrayList<String>();
		try {
			Connection conn = DriverManager.getConnection(url); 
			Statement stmt = conn.createStatement();
			String sql = "Select SHOW,SEASON,EPISODE FROM WATCHLIST;"; 
	        ResultSet rs = stmt.executeQuery(sql);
	        while (rs.next()) {
	            String show = rs.getString("SHOW");
	            String season = rs.getString("SEASON");
	            String episode = rs.getString("EPISODE");
	            list.add(show + ":" + season + ":" + episode);
	        }
	        conn.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		if(descending) {
			Collections.sort(list, Collections.reverseOrder());
		}else {
			Collections.sort(list);
		}
		return list;
	}
	
	public synchronized String viewWatchList() {
		StringBuilder sb = null;
		try {
			Connection conn = DriverManager.getConnection(url); 
			Statement stmt = conn.createStatement();
			String sql = "Select * FROM WATCHLIST;"; 
	        ResultSet rs = stmt.executeQuery(sql);
	        sb = new StringBuilder();
	        while (rs.next()) {
	            String show = rs.getString("SHOW");
	            String season = rs.getString("SEASON");
	            String episode = rs.getString("EPISODE");
	            sb.append(show + "\t" + season + "\t" + episode +"\n");
	        }
	        conn.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		if(sb!=null) {
			return sb.toString();
		}else {
			return null;
		}
	}
	
	public synchronized List<String> getUnwatched(boolean descending){
		List<String> list = new ArrayList<String>();
		try {
			Connection conn = DriverManager.getConnection(url); 
			Statement stmt = conn.createStatement();
			String sql = "Select SHOW,SEASON,EPISODE,NAME FROM UNWATCHED;"; 
	        ResultSet rs = stmt.executeQuery(sql);
	        while (rs.next()) {
	            String show = rs.getString("SHOW");
	            int season = rs.getInt("SEASON");
	            int episode = rs.getInt("EPISODE");
	            String name = rs.getString("NAME");
	            list.add(show + ":" + season + ":" + episode + ":" + name);
	        }
	        conn.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		if(descending) {
			Collections.sort(list, Collections.reverseOrder());
		}else {
			Collections.sort(list);
		}
		return list;
	}
	
	public synchronized String viewUnwatchedList() {
		StringBuilder sb = null;
		try {
			Connection conn = DriverManager.getConnection(url); 
			Statement stmt = conn.createStatement();
			String sql = "Select * FROM UNWATCHED;"; 
	        ResultSet rs = stmt.executeQuery(sql);
	        sb = new StringBuilder();
	        while (rs.next()) {
	            String show = rs.getString("SHOW");
	            int season = rs.getInt("SEASON");
	            int episode = rs.getInt("EPISODE");
	            String name = rs.getString("NAME");
	            sb.append(show + "\t" + season + "\t" + episode + "\t" + name + "\n");
	        }
	        conn.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		if(sb!=null) {
			return sb.toString();
		}else {
			return null;
		}
	}
}
