package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import db.DBInterface;

class DBInterfaceTest {
	private static DBInterface db;
	@BeforeEach
	public void init() {
		File dbFile = new File("test/db/data.db");
		if(dbFile.exists()) {
			dbFile.delete();
		}
		DBInterface.url = "jdbc:sqlite:test/db/data.db"; 
		DBInterface.path = "test/db/data.db";
		DBInterface.resetInstance();
		db = DBInterface.getInstance();
		
	}
	
	@Test
	void addEpisodeRight() {
		System.out.println("................addEpisodeRightTest................");
		db.addUnwatched("testShow", 1, 1, "testEpisode");
		db.addEpisode("testShow", "testSeason", "testEpisode");
		assertEquals("testShow\ttestSeason\ttestEpisode\n", db.viewWatchList());
	}
	@Test
	void addUnwatchedRight() {
		System.out.println("................addUnwatchedRightTest................");
		db.addUnwatched("testShow", 1, 1, "testEpisode");
		System.out.println(db.viewWatchList());
		assertEquals("testShow\t1\t1\ttestEpisode\n", db.viewUnwatchedList());
	}

}
